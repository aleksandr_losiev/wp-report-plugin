<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       mailto:tk@wwwest.solutions
 * @since      1.0.0
 *
 * @package    Woo_Revenue
 * @subpackage Woo_Revenue/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Woo_Revenue
 * @subpackage Woo_Revenue/includes
 * @author     Taras Kumpanenko <tk@wwwest.solutions>
 */
class Woo_Revenue
{

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Woo_Revenue_Loader $loader Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string $plugin_name The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string $version The current version of the plugin.
     */
    protected $version;

    private $cron;
    private static $plugin_opt_key;
    private $pdf;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct()
    {
        define('ADMIN_DIR', plugin_dir_path(dirname(__FILE__)) . 'admin/');

        if (defined('PLUGIN_NAME_VERSION')) {
            $this->version = PLUGIN_NAME_VERSION;
        } else {
            $this->version = '1.0.0';
        }
        $this->plugin_name = 'woo-revenue';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();

//        add_action('init', array($this, 'after_wp_loaded'));
    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Woo_Revenue_Loader. Orchestrates the hooks of the plugin.
     * - Woo_Revenue_i18n. Defines internationalization functionality.
     * - Woo_Revenue_Admin. Defines all hooks for the admin area.
     * - Woo_Revenue_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies()
    {

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-woo-revenue-loader.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-woo-revenue-i18n.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-woo-revenue-admin.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/woo-revenue-ajax-actions.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-woo-cron-controller.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-woo-revenue-pdf-generator.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-woo-revenue-orders.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-woo-revenue-products.php';

        $this->loader = new Woo_Revenue_Loader();


    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Woo_Revenue_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */

//    public function after_wp_loaded()
//    {
//        $this->cron = new WooCronController('Woo_Revenue::send_email');
//        $this->pdf = new WooRevenuePdfGenerator(array('key'=>'1'));
//        die();
//    }

//    public static function send_email()
//    {
//        $plugins_options = get_option(self::$plugin_opt_key);
//        wp_mail($plugins_options['reporter_email'], 'Woo Revenue daily report', 'I made a cron and will send emails with one min interval. BITCHES');
//    }

    private function set_locale()
    {

        $plugin_i18n = new Woo_Revenue_i18n();

        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');

    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks()
    {

        $plugin_admin = new Woo_Revenue_Admin($this->get_plugin_name(), $this->get_version());
        $this::$plugin_opt_key = $plugin_admin->get_options_name();
    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_public_hooks()
    {

//        $plugin_public = new Woo_Revenue_Public($this->get_plugin_name(), $this->get_version());
//
//        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
//        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');

    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run()
    {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */
    public function get_plugin_name()
    {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    Woo_Revenue_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader()
    {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function get_version()
    {
        return $this->version;
    }

}
