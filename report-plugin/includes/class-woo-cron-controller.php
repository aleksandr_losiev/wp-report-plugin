<?php

class WooCronController
{
    private $cron_name = 'send_daily_report';
    private $cron_hook = 'send_report_hook';
    private $recurrence = 'hourly';
    private $callback;

    function __construct($callback)
    {
        $this->callback = $callback;
        add_filter('cron_schedules', array($this, 'setup_cron_schedule'));
        add_action($this->cron_hook, function () {
            call_user_func($this->callback);
        });

        $this->setup_cron();
    }

    private function setup_cron()
    {
        if (!wp_next_scheduled($this->cron_hook)) {
            wp_schedule_event(time(), $this->recurrence, $this->cron_hook);
        }
    }


    function setup_cron_schedule($schedules)
    {
        $schedules[$this->cron_name] = array('interval' => $this->recurrence, 'display' => 'Daily Woo Revenue Report');
        return $schedules;
    }

}