<?php

/**
 * Fired during plugin activation
 *
 * @link       mailto:tk@wwwest.solutions
 * @since      1.0.0
 *
 * @package    Woo_Revenue
 * @subpackage Woo_Revenue/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Woo_Revenue
 * @subpackage Woo_Revenue/includes
 * @author     Taras Kumpanenko <tk@wwwest.solutions>
 */
class Woo_Revenue_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
