<?php

/**
 * Fired during plugin deactivation
 *
 * @link       mailto:tk@wwwest.solutions
 * @since      1.0.0
 *
 * @package    Woo_Revenue
 * @subpackage Woo_Revenue/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Woo_Revenue
 * @subpackage Woo_Revenue/includes
 * @author     Taras Kumpanenko <tk@wwwest.solutions>
 */
class Woo_Revenue_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
