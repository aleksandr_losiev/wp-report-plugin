<?php

class WooRevenueProducts{

    public $productsPerPage = 40;
    public $offset = 0;
    public $productsCount;

    public function __construct()
    {
        $this->productsCount = count($this->getProducts(0,-1, 'ids'));
    }

    public function getProducts($offset = null, $limit = null, $fields = null)
    {
        $offset = $offset ?: $this->offset;
        $limit = $limit ?: $this->productsPerPage;
//        $fields = $fields ?: '';

        $args = array(
//            'status' => 'all',
            'limit' => $limit,
            'offset' => $offset
        );

        if ($fields) $args['return'] = $fields;

        return wc_get_products( $args );
    }

    public function getEditorPermalink($post_id = null)
    {
        if (!$post_id) return '';

        return '/wp-admin/post.php?post=' . $post_id . '&action=edit';
    }
}