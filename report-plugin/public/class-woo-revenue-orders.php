<?php

class WooRevenueOrders
{
    public $firstOrderDate;
    public $paymentMethods = [];
    public $taxVariable = [];

    public function __construct()
    {
        $this->firstOrderDate = $this->getFirstOrderDate();
    }

    public function getFirstOrderData()
    {
        $args = array(
            'posts_per_page' => 1,
            'orderby' => 'date',
            'order' => 'ASC',
        );

        $order = wc_get_orders($args);

        return isset($order[0]) ? $order[0] : 0;

    }

    public function getFirstOrderDate()
    {
        $firstOrder = $this->getFirstOrderData();
        $date = ((array)$firstOrder->get_date_created())['date'];
        $date = new DateTime($date);

        return $date->format('Y-m-d');
    }

    public function getDateListByMonth()
    {
        $list = [];

        $dateToday = new DateTime();
        $dateToday->modify('first day of this month');
        $firstDayThisMonth = $dateToday->format('Y-m-d');
        $dateToday->modify('last day of this month');
        $lastDayThisMonth = $dateToday->format('Y-m-d');
        $lastMonth = $firstDayThisMonth . '...' . $lastDayThisMonth;

        $startDate = new DateTime($this->firstOrderDate);
        $startDate->modify('first day of this month');
        $firstStartDay = $startDate->format('Y-m-d');

        while ($firstStartDay != $firstDayThisMonth){
            $startDate->modify('last day of this month');
            $lastStartDay = $startDate->format('Y-m-d');

            $list[$startDate->format('Y-m')] = $firstStartDay . '...' . $lastStartDay;
            $startDate->modify('+1 day');
            $firstStartDay = $startDate->format('Y-m-d');
        }

        $list[$dateToday->format('Y-m')] = $lastMonth;

        return array_reverse($list);
    }

    /**
     * @param $date {string} Y-m-d
     * @return array
     */
    public function getDateBetweenByDate($date)
    {
        if (!$date) return [];

        $date = new DateTime($date);
        $date->modify('first day of this month');
        $start = $date->format('Y-m-d');
        $date->modify('last day of this month');
        $end = $date->format('Y-m-d');
        $month = $date->format('Y-m');

        return [
            'month' => $month,
            'between' => $start . '...' . $end,
        ];
    }

    public function getOrdersDataByMonth($date_between)
    {
        $args = array(
            'posts_per_page' => -1,
            'date_created' => $date_between,
        );

        return wc_get_orders($args);
    }

    public function getMonthStatistic($date_between, $orders_data = []){
        $result = [
            'total'  => 0,
            'tax'    => [
                'tax_7'  => 0,
                'tax_19' => 0,
                'tax_other' => 0,
            ],
            'chanel'  => [
                'website' => [
                    'total' => 0,
                    'invoice' => 0,
                    'payments_advanced' => 0,
                    'paypal' => 0,
                    'amazon' => 0,
                    'sofortgateway' => 0,
                    'bacs' => 0,
                    'cod' => 0,
                ],
                'amazon'  => [
                    'total' => 0,
                ],
                'ebay'    => [
                    'total' => 0,
                ],
            ],

        ];

        $ordersData = empty($orders_data) ? $this->getOrdersDataByMonth($date_between) : $orders_data;

        foreach ($ordersData as $key => $order){

            $orderData = $this->getOrderStatistic($order);

            if (!$orderData['total'] || !$orderData['created_via']) {
                continue;
            }

            $result['total'] += $orderData['total'];

            if (!in_array($orderData['payment_method'], $this->paymentMethods)) {
                $this->paymentMethods[] = $orderData['payment_method'];
            }

            if (array_key_exists('tax_' . $orderData['tax_percent_round'], $this->taxVariable)) {
                $this->taxVariable['tax_' . $orderData['tax_percent_round']] += 1;
            } else {
                $this->taxVariable['tax_' . $orderData['tax_percent_round']] = 1;
            }

            $orderTotal = round($orderData['total'],2);

            switch ($orderData['created_via']){
                case 'checkout':
                    $result['chanel']['website']['total'] += $orderTotal;
                    switch ($orderData['payment_method']){
                        case 'paypal':
                            $result['chanel']['website']['paypal'] += $orderTotal;
                            break;
                        case 'amazon_payments_advanced':
                            $result['chanel']['website']['amazon'] += $orderTotal;
                            break;
                        case 'sofortgateway':
                            $result['chanel']['website']['sofortgateway'] += $orderTotal;
                            break;
                        case 'invoice':
                            $result['chanel']['website']['invoice'] += $orderTotal;
                            break;
                        case 'cod':
                            $result['chanel']['website']['cod'] += $orderTotal;
                            break;
                        case 'bacs':
                            $result['chanel']['website']['bacs'] += $orderTotal;
                            break;
                    }
                    break;
                case 'amazon':
                    $result['chanel']['amazon']['total'] += $orderTotal;
                    break;
                case 'ebay':
                    $result['chanel']['ebay']['total'] += $orderTotal;
                    break;
            }

            switch ($orderData['tax_percent_round']){
                case 6:
                case 7:
                case 8:
                    $result['tax']['tax_7'] += $orderData['cart_tax'];
                    break;
                case 18:
                case 19:
                case 20:
                    $result['tax']['tax_19'] += $orderData['cart_tax'];
                    break;
                default :
                    $result['tax']['tax_other'] += $orderData['cart_tax'];
                    break;
            }

        }
        return $result;
    }

    public function getTotalStatistic()
    {
        $result = [];
        $counter = 0;
//        var_dump($this->getDateListByMonth());

        foreach ($this->getDateListByMonth() as $key => $dateBetween){
            if ($counter < 2){
                $result[$key] = $this->getMonthStatistic($dateBetween);
            } else {
                $monthData = get_transient($dateBetween . '_orders');
                if ($monthData){
                    $result[$key] = $monthData;
                }
            }

            $counter++;
        }

        return $result;
    }

    public function getOrderStatistic($order)
    {
        $result = [
            'id' => $order->get_id(),
            'url' => $this->getOrderUrl($order->get_id()),
            'total' => round($order->get_total(), 2),
            'shipping_total' => round($order->get_shipping_total(),2),
            'currency' => $order->get_currency(),
            'status' => $order->get_status(),
            'date_created' => $order->get_date_created()->date('Y-m-d'),
            'payment_method' => method_exists($order, 'get_payment_method') ? $order->get_payment_method() : '',
            'created_via' => method_exists($order, 'get_created_via') ? $order->get_created_via() : '',
            'cart_tax' => round($order->get_cart_tax(),2),
            'total_tax' => round($order->get_total_tax(),2),
            'shipping_tax' => round($order->get_shipping_tax(),2),
        ];

        $without_tax = $result['total'] - $result['shipping_total'] - $result['total_tax'];
        $tax_percent = $without_tax ? $result['cart_tax'] / $without_tax * 100 : 0;

        $result += [
            'without_tax' => round($without_tax, 2),
            'tax_percent' => round($tax_percent,2),
            'tax_percent_round' => round($tax_percent),
        ];

        return $result;
    }

    public function getIncorrectTax()
    {
        $counter = 0;
        $result = [];
        foreach ($this->getDateListByMonth() as $key => $dateBetween){
            if ($counter < 3){
                $result = array_merge($result, $this->getIncorrectTaxByMonth($dateBetween));
            } else {
                $incorrectTaxByMonth = get_transient($dateBetween . '_incorrect_tax');
                if ($incorrectTaxByMonth){
                    $result = array_merge($result, $incorrectTaxByMonth);
                }
            }
            $counter++;
        }

        return $result;
    }

    public function getIncorrectTaxByMonth($date_between, $orders_data = [])
    {
        $result = [];
        $ordersData = empty($orders_data) ? $this->getOrdersDataByMonth($date_between) : $orders_data;

        foreach ($ordersData as $key => $order){
            $orderData = $this->getOrderStatistic($order);

            if (!$orderData['total'] || !$orderData['created_via']) {
                continue;
            }

            switch ($orderData['tax_percent_round']){
                case 0:
                case 6:
                case 7:
                case 8:
                case 18:
                case 19:
                case 20:
                    continue;
                    break;
                default:
                    $result[$orderData['id']] = $orderData;
                    break;
            }
        }

        return $result;
    }


    public function getOrderUrl($order_id)
    {
        return '/wp-admin/post.php?post=' . $order_id . '&action=edit';
    }


    public function getOrdersCount()
    {
        $args = array(
            'posts_per_page' => -1,
            'return' => 'ids',
        );

        return count(wc_get_orders($args));
    }

    public function getOrders($offset = 0, $limit = 30)
    {
        $args = [
            'posts_per_page' => $limit,
            'offset' => $offset
        ];

        return wc_get_orders($args);
    }


}

?>