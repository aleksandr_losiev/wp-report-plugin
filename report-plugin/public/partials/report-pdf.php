<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       mailto:tk@wwwest.solutions
 * @since      1.0.0
 *
 * @package    Woo_Revenue
 * @subpackage Woo_Revenue/public/partials
 */
?>

<?php $orders = new WooRevenueOrders(); ?>

<style type="text/css">
    #main table{
        margin-top: 40px;
        width: 100%;
        padding: 20px 0;
        font-size: 14px;
        border-collapse: collapse;
    }
    #main table,
    #main th,
    #main td{
        border: 1px solid black;
    }
    #main table td{
        padding: 3px 5px;
    }
    #main table thead tr{
        background-color: #636363;
        color: white;
        font-weight: bold;
    }
    #main table thead tr:last-child{
        background-color: #ecd5c1;
        color: #111111;
        font-weight: normal;
    }
    #main table thead td{
        text-align: center;
    }
</style>

<div id="main">
    <table>
        <thead>
        <tr>
            <td>Month</td>
            <td colspan="4">Revenue</td>
            <td colspan="10">Channels</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="3">Tax</td>
            <td colspan="8">Website</td>
            <td>amazon</td>
            <td>ebay</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>Total</td>
            <td>7%</td>
            <td>19 %</td>
            <td>Other</td>
            <td>Total</td>
            <td>Pay. in advance</td>
            <td>Invoice</td>
            <td>Paypal</td>
            <td>Amazon</td>
            <td>Sofort&uuml;ber...</td>
            <td>Bacs</td>
            <td>Cod</td>
            <td>Amazon</td>
            <td>Ebay</td>
        </tr>
        </thead>

        <?php foreach ($orders->getTotalStatistic() as $key => $monthOrders): ?>
            <tr>
                <td><?= $key ?></td>
                <td><?= round($monthOrders['total'], 2) ?></td>
                <td><?= round($monthOrders['tax']['tax_7'], 2) ?></td>
                <td><?= round($monthOrders['tax']['tax_19'], 2) ?></td>
                <td><?= round($monthOrders['tax']['tax_other'], 2) ?></td>
                <td><?= round($monthOrders['chanel']['website']['total'], 2) ?></td>
                <td><?= round($monthOrders['chanel']['website']['payments_advanced'], 2) ?></td>
                <td><?= round($monthOrders['chanel']['website']['invoice'], 2) ?></td>
                <td><?= round($monthOrders['chanel']['website']['paypal'], 2) ?></td>
                <td><?= round($monthOrders['chanel']['website']['amazon'], 2) ?></td>
                <td><?= round($monthOrders['chanel']['website']['sofortgateway'], 2) ?></td>
                <td><?= round($monthOrders['chanel']['website']['bacs'], 2) ?></td>
                <td><?= round($monthOrders['chanel']['website']['cod'], 2) ?></td>
                <td><?= round($monthOrders['chanel']['amazon']['total'],2) ?></td>
                <td><?= round($monthOrders['chanel']['ebay']['total'], 2)  ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>
