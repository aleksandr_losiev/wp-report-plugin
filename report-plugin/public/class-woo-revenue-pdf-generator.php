<?php

require_once dirname(__FILE__) . '/../vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class WooRevenuePdfGenerator
{
    private $pdf_folder;
    private $filename;
    private $folder_name = 'reports-pdf';

    function __construct($data)
    {
        $this->pdf_folder = wp_upload_dir()['basedir'] . '/' . $this->folder_name;
        if (!file_exists($this->pdf_folder)) {
            mkdir($this->pdf_folder, 0777, true);
        }
        $this->filename = date('d.m.Y', time()) . '_' . time() . '.pdf';
        $this->generate_pdf($data);
    }

    function generate_pdf($data)
    {
        try {
            ob_start();
            include dirname(__FILE__) . '/partials/report-pdf.php';
            $content = ob_get_clean();
            $html2pdf = new Html2Pdf('L', 'A4', 'en');
            $html2pdf->writeHTML($content);
            $html2pdf->output($this->get_pdf_file_path(), 'F');
        } catch (Html2PdfException $e) {
            $html2pdf->clean();
            $formatter = new ExceptionFormatter($e);
            echo $formatter->getHtmlMessage();
        }
    }

    public function get_pdf_file_path()
    {
        return $this->pdf_folder . '/' . $this->filename;
    }

    public function get_pdf_url()
    {
        return wp_upload_dir()['baseurl'] . '/' . $this->folder_name . '/' . $this->filename;
    }
}