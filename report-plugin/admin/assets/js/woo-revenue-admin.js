(function( $ ) {

    /**
     *  Start Timepicker
     */

    let timepicker = $('.timepicker');

    if (timepicker.length) {
        timepicker.wickedpicker({
            now: cronData.time,
            twentyFour: true,
            minutesInterval: 10
        });

        $('body').on('click', '.wickedpicker .wickedpicker__controls__control span', function () {
            timepicker.attr('value', timepicker.wickedpicker('time'));
        });
    }

    /**
     * Start Radio button checked
     */


    /**
     *  End timepicker
     */

    let cronRadioButtonBlock = $('.cron-radio-button'),
        cronActionFieldField = $('.action-field');

    let cronRadioButtonField = cronRadioButtonBlock.find('.single-radio input');

    $(cronRadioButtonField).click(function () {
        console.log($(this).val());
        cronActionFieldField.addClass('hidden');
        $('.action-field[data-action="' + $(this).val() + '"]').removeClass('hidden');

    });

    /**
     * End Radio button checked
     */

    /**
     * CRON
     */

    $('#start_cron_button').click(function (e) {
        e.preventDefault();

        let action = getActionButtonElements($(this));
        let cronData = {
            time: timepicker.wickedpicker('time'),
            interval_type : $('input[name="woo_revenue_opt[cron_interval]"]:checked').val(),
            interval_week: $('.cron-field select').val(),
            interval_month: $('.cron-field[data-action="month_day"] input').val(),
            interval_day: $('.cron-field[data-action="interval_day"] input').val(),
        };

        if (action.buttonBlock.hasClass('active')) return;

        changeStatusFirstSrep(action);

        new Promise((resolve, reject) => {
            resolve(startCron(cronData));
        }).then(result => {
            console.log(result);
            action.statusIndicator.addClass('hidden');
            action.statusDescription.html(action.text.finish);
            action.buttonBlock.removeClass('active');
        });


    });

    /**
     * End CRON
     */


    /**
     * Action for Cache
     */
    $('#upload_orders_to_cache').click(function (e) {
		e.preventDefault();

		let monthCount = 0;
		let monthCounter = 0;
		let $cacheBlockButtonBlock = $('.caching_orders_button');
		let $statusBlock = $cacheBlockButtonBlock.find('.upload_orders_status');
		let $statusBlockDesc = $statusBlock.find('.description');
		let $statusBlockIndicator = $statusBlock.find('.load-icon');

		if ($cacheBlockButtonBlock.hasClass('active')) return;

        $cacheBlockButtonBlock.addClass('active');
        $statusBlockDesc.html('Start caching');
		$statusBlock.removeClass('hidden');
        $statusBlockIndicator.removeClass('hidden');

        new Promise((resolve, reject) => {
        	resolve(getListOfTheMonth());
		}).then(resolve => {
            $statusBlockDesc.html('Collection of information on Orders');
            let monthArray = JSON.parse(resolve);
            monthCount = Object.keys(monthArray).length;
            // console.log(monthArray);
            $.each(monthArray, function(index, value) {
                new Promise((resolve) => {
                    resolve(uploadOrdersInCache(value))
                }).then(resolve => {
                    monthCounter++;
                    $statusBlockDesc.html('Load <b>' + monthCounter + '</b> from <b>' + monthCount + '</b>');
                    console.log(resolve);

                    if (monthCounter === monthCount){
                        $statusBlockIndicator.addClass('hidden');
                        $statusBlockDesc.html('Finished!');
					}
                });
            });
		});
    });

    $('#order_cache_clear').click(function (e) {
        e.preventDefault();

        let monthCount = 0,
            monthCounter = 0,
            additionalText = {
                step2: 'data-text-step2'
            };
        let action = getActionButtonElements($(this), additionalText);

        if (action.buttonBlock.hasClass('active')) return;

        changeStatusFirstSrep(action);


        new Promise((resolve, reject) => {
            resolve(getListOfTheMonth());
        }).then(resolve => {
            action.statusDescription.html(action.text.step2);
            let monthArray = JSON.parse(resolve);
            monthCount = Object.keys(monthArray).length;
            $.each(monthArray, function(index, value) {
                new Promise((resolve) => {
                    resolve(removeOrdersInCache(value))
                    // resolve(monthCounter);
                }).then(resolve => {
                    monthCounter++;
                    action.statusDescription.html('Removed <b>' + monthCounter + '</b> from <b>' + monthCount + '</b>');
                    console.log(resolve);

                    if (monthCounter === monthCount){
                        action.statusIndicator.addClass('hidden');
                        action.statusDescription.html(action.text.finish);
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    }
                });
            });
        });
    });
    /**
     * End action for Cache
     */

	$('#save_pdf_button').click(function (e) {
        e.preventDefault();

        let $cacheBlockButtonBlock = $('.save_pdf_block');
        let $statusBlock = $cacheBlockButtonBlock.find('.save_pdf_status');
        let $statusBlockDesc = $statusBlock.find('.description');
        let $statusBlockIndicator = $statusBlock.find('.load-icon');

        if ($cacheBlockButtonBlock.hasClass('active')) return;

        $cacheBlockButtonBlock.addClass('active');
        $statusBlockDesc.html('Start making PDF');
        $statusBlock.removeClass('hidden');
        $statusBlockIndicator.removeClass('hidden');

        new Promise((resolve, reject) => {
            resolve(save_pdf());
        }).then(result => {
        	console.log(JSON.parse(result));
            $statusBlockIndicator.addClass('hidden');
            $statusBlockDesc.html('<a href="'+ JSON.parse(result) +'" download>Download PDF</a>');
            // $cacheBlockButtonBlock.find('a').attr('href',JSON.parse(result)).trigger('click');

        });
    });

	$('#incorrect-tax-save, #incorrect-tax-update').click(function (e) {
        e.preventDefault();

        let monthCount = 0,
            monthCounter = 0,
            action = getActionButtonElements($(this));

        if (action.buttonBlock.hasClass('active')) return;

        changeStatusFirstSrep(action);

        new Promise((resolve, reject) => {
            resolve(getListOfTheMonth());
        }).then(resolve => {
            action.statusDescription.html('Collection of information on Orders');
            let monthArray = JSON.parse(resolve);
            monthCount = Object.keys(monthArray).length;
            $.each(monthArray, function(index, value) {
                new Promise((resolve) => {
                    resolve(uploadIncorrectOrdersInCache(value, action.isUpdate))
                }).then(resolve => {
                    monthCounter++;
                    action.statusDescription.html('Load <b>' + monthCounter + '</b> from <b>' + monthCount + '</b>');
                    console.log(resolve);

                    if (monthCounter === monthCount){
                        action.statusIndicator.addClass('hidden');
                        action.statusDescription.html(action.text.finish);
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    }
                });
            });
        });

    });

	function getActionButtonElements(that, additional_text = null) {
	    let buttonBlock = that.closest('.action-button-block');

	    let textData = {
            start: that.attr('data-text-start'),
            finish: that.attr('data-text-finish')
        };

	    if (additional_text.length){
            for (let key in additional_text) {
                textData[key] = that.attr(additional_text[key]);
            }
        }

        return {
            buttonBlock: buttonBlock,
            statusBlock: buttonBlock.find('.action-status'),
            statusDescription: buttonBlock.find('.description'),
            statusIndicator: buttonBlock.find('.load-icon'),
            isUpdate: !!that.attr('data-is-update'),
            text: textData
        }
    }

    function changeStatusFirstSrep(elements) {
        elements.buttonBlock.addClass('active');
        elements.statusDescription.html(elements.text.start);
        elements.statusBlock.removeClass('hidden');
        elements.statusIndicator.removeClass('hidden');
    }


    /**
     *  Ajax functions
     */

	function uploadOrdersInCache(date_between) {
		let data = {
			action: 'upload_orders_in_cache',
            date_between: date_between
		};

		return $.post(ajaxurl, data, function (response) {
			return response;
        })
    }

    function getListOfTheMonth() {
        let data = {
            action: 'get_list_of_the_month'
        };

        return $.post(ajaxurl, data, function (response) {
            return response;
        })
    }

    function save_pdf() {
        let data = {
            action: 'save_pdf'
        };

        return $.post(ajaxurl, data, function (response) {
            return response;
        })
    }

    function uploadIncorrectOrdersInCache(date_between, update = false) {
        let data = {
            action: 'upload_incorrect_orders_in_cache',
            date_between: date_between,
            is_update: update

        };

        return $.post(ajaxurl, data, function (response) {
            return response;
        })
    }

    function removeOrdersInCache(date_between) {
        let data = {
            action: 'remove_orders_in_cache',
            date_between: date_between
        };

        return $.post(ajaxurl, data, function (response) {
            return response;
        })
    }

    function startCron($cron_data) {
        let data = {
            action: 'start_cron',
            form_data: $cron_data
        };

        return $.post(ajaxurl, data, function (response) {
            return response;
        })
    }




})( jQuery );
