<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       mailto:tk@wwwest.solutions
 * @since      1.0.0
 *
 * @package    Woo_Revenue
 * @subpackage Woo_Revenue/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Woo_Revenue
 * @subpackage Woo_Revenue/admin
 * @author     Taras Kumpanenko <tk@wwwest.solutions>
 */
class Woo_Revenue_Admin
{

    private $plugin_name;
    private $version;
    private $options;
    private $plugin_options_name = 'woo_revenue_opt';

    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

        add_action('admin_menu', array($this, 'add_plugin_page'));
        add_action('admin_init', array($this, 'page_init'));
        add_action('admin_enqueue_scripts', array($this, 'load_scripts'));

        class_exists('WooRevenueAjaxActions') && new WooRevenueAjaxActions();

    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Woo Revenue Admin section',
            'Woo Revenue Reporter',
            'manage_options',
            'wrg-setting-admin',
            array($this, 'create_admin_page')
        );

        add_submenu_page(
            null,
            'Woo Revenue Orders table',
            'Woo Revenue Orders table',
            'manage_options',
            'wrg-setting-orders-table',
            array($this, 'create_orders_table_page')
        );

        add_submenu_page(
            null,
            'Woo Revenue Incorrect taxonomy',
            'Woo Revenue Incorrect taxonomy',
            'manage_options',
            'wrg-setting-incorrect-taxonomy',
            array($this, 'create_incorrect_taxonomy')
        );

        add_submenu_page(
            null,
            'Woo Revenue Products table',
            'Woo Revenue Products table',
            'manage_options',
            'wrg-setting-products-table',
            array($this, 'create_products_table')
        );

        add_submenu_page(
            null,
            'Woo Revenue Custom table',
            'Woo Revenue Custom table',
            'manage_options',
            'wrg-setting-custom-table',
            array($this, 'create_custom_table')
        );
    }

    public function create_admin_page()
    {
        $this->options = get_option($this->plugin_options_name);
        ob_start();
        include_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/woo-revenue-admin-display.php';
        $content = ob_get_clean();
        echo $content;
    }

    public function create_orders_table_page()
    {
        $this->options = get_option($this->plugin_options_name);
        ob_start();
        include_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/woo-revenue-orders-table.php';
        $content = ob_get_clean();
        echo $content;
    }

    public function create_incorrect_taxonomy()
    {
        $this->options = get_option($this->plugin_options_name);
        ob_start();
        include_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/woo-revenue-incorrect-taxonomy.php';
        $content = ob_get_clean();
        echo $content;
    }

    public function create_products_table()
    {
        $this->options = get_option($this->plugin_options_name);
        ob_start();
        include_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/woo-revenue-products-table.php';
        $content = ob_get_clean();
        echo $content;
    }

    public function create_custom_table()
    {
        $this->options = get_option($this->plugin_options_name);
        ob_start();
        include_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/woo-revenue-custom-table.php';
        $content = ob_get_clean();
        echo $content;
    }


    public function page_init()
    {
        register_setting('wrg_option_group', $this->plugin_options_name, array($this, 'sanitize'));

        add_settings_section('setting_section_id', null, array(), 'wrg-setting-admin');
        add_settings_field('reporter_email',__('Reporter email', 'woo-revenue'), array($this, 'reporter_email_callback'), 'wrg-setting-admin', 'setting_section_id');
        add_settings_field('cache_month', __('Upload orders to cache', 'woo-revenue'), array($this, 'reporter_cache_orders'), 'wrg-setting-admin', 'setting_section_id');
        add_settings_field('cache_clear', __('Deleting the order cache', 'woo-revenue'), array($this, 'reporter_clear_cache_orders'), 'wrg-setting-admin', 'setting_section_id');
        add_settings_field('save_pdf', __('Save table in PDF', 'woo-revenue'), array($this, 'reporter_save_pdf'), 'wrg-setting-admin', 'setting_section_id');
        add_settings_field('cron_data', __('Time & date to send email', 'woo-revenue'), array($this, 'cronData'), 'wrg-setting-admin', 'setting_section_id');


        add_settings_section('section_orders_table', null, array($this, 'getOrdersTable'), 'wrg-setting-orders-table');

        add_settings_section('section_incorrect_taxonomy', null, array($this, 'incorrectTaxonomy'), 'wrg-setting-incorrect-taxonomy');

        add_settings_section('section_products_table', null, array($this, 'getProductsTable'), 'wrg-setting-products-table');

        add_settings_section('section_custom_table', null, array($this, 'getCustomTable'), 'wrg-setting-custom-table');
    }

    public function sanitize($input)
    {
        $new_input = array();
        if (isset($input['reporter_email']))
            $new_input['reporter_email'] = absint($input['reporter_email']);

        return $input;
    }


    public function reporter_email_callback()
    {
        printf(
            '<input type="email" id="reporter_email" name="' . $this->plugin_options_name . '[reporter_email]" value="%s" />',
            isset($this->options['reporter_email']) ? esc_attr($this->options['reporter_email']) : ''
        );
    }

    public function reporter_cache_orders()
    {
        echo    '<div class="caching_orders_button">
                    <button id="upload_orders_to_cache" class="button button-secondary">' . __("Upload", "woo-revenue"). '</button>
                    <div class="upload_orders_status hidden">Status: <span class="description"></span><span class="dashicons dashicons-update load-icon hidden"></span></div>
                </div>';
    }

    public function reporter_clear_cache_orders()
    {
        $id = 'order_cache_clear';
        $button = __('Clear Cache', 'woo-revenue');
        $data = [
            'data-text-start' => __('Start remove cache', 'woo-revenue'),
            'data-text-step2' => __('Deleting and cleaning the cache', 'woo-revenue'),
            'data-text-finish' => __('Cache removed successfully!', 'woo-revenue'),
        ];

        echo $this->ajax_action_button_html($button, $id, $data);
    }

    public function reporter_save_pdf()
    {
        echo    '<div class="save_pdf_block">
                    <button id="save_pdf_button" class="button button-secondary">Generate PDF</button>
                    <div class="save_pdf_status hidden">Status: <span class="description"></span><span class="dashicons dashicons-update load-icon hidden"></span></div>
                    <a href="#" download></a>
                </div>';
    }

    public function get_options_name()
    {
        return $this->plugin_options_name;
    }

    public function cronData()
    {
        ob_start();
        include plugin_dir_path(dirname(__FILE__)) . 'admin/partials/template-parts/woo-revenue-cron-data.php';
        $content = ob_get_clean();

        echo $content;
    }

    public function getOrdersTable()
    {
        ob_start();
        include plugin_dir_path(dirname(__FILE__)) . 'public/partials/report-pdf.php';
        $content = ob_get_clean();

        echo $content;
    }


    public function getProductsTable()
    {
        ob_start();
        include plugin_dir_path(dirname(__FILE__)) . 'admin/partials/tables/woo-revenue-products-table.php';
        $content = ob_get_clean();

        echo $content;
    }


    public function getCustomTable()
    {
        ob_start();
        include plugin_dir_path(dirname(__FILE__)) . 'admin/partials/tables/woo-revenue-custom-table.php';
        $content = ob_get_clean();

        echo $content;
    }

    public function incorrectTaxonomy()
    {
        ob_start();
        include plugin_dir_path(dirname(__FILE__)) . 'admin/partials/tables/woo-revenue-incorrect-taxonomy-table.php';
        $content = ob_get_clean();

        echo $content;
    }

    public function load_scripts()
    {
        wp_enqueue_style('woo-revenue',  plugin_dir_url( __FILE__ ) . 'assets/css/woo-revenue-admin.css', false, '1.0.0');
        wp_enqueue_style('wickedpicker',  plugin_dir_url( __FILE__ ) . 'assets/css/wickedpicker.min.css', false, '2.2.4');

        wp_enqueue_script('wickedpicker',  plugin_dir_url( __FILE__ ) . 'assets/js/wickedpicker.min.js', ['jquery'], '2.2.4', true);
        wp_enqueue_script('woo-revenue',  plugin_dir_url( __FILE__ ) . 'assets/js/woo-revenue-admin.js', ['jquery'], '1.0.0', true);
    }

    public function ajax_action_button_html($AB_Text, $AB_ID, $AB_Data, $AB_HelperContent = '')
    {
        ob_start();
            include plugin_dir_path(dirname(__FILE__)) . 'admin/partials/template-parts/woo-revenue-action-button.php';
        return ob_get_clean();
    }

}
