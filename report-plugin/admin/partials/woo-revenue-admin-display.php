<div class="wrap">
    <h2><?php _e('Woocommerce Revenue Reports','woo-revenue'); ?></h2>
    <?php include 'template-parts/woo-revenue-orders-menu.php' ?>

    <form method="post" action="options.php">
        <?php
            settings_fields( 'wrg_option_group' );
            do_settings_sections( 'wrg-setting-admin' );
            submit_button();
        ?>
    </form>
</div>
