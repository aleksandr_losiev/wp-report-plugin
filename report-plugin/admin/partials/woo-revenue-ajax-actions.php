<?php

//use WooRevenueOrders;
class WooRevenueAjaxActions {

    public $orderClass;

    public function __construct()
    {
        add_action('wp_ajax_upload_orders_in_cache', array($this, 'uploadOrdersInCache'));
        add_action('wp_ajax_remove_orders_in_cache', array($this, 'removeOrdersInCache'));
        add_action('wp_ajax_get_list_of_the_month', array($this, 'getListOfTheMonth'));
        add_action('wp_ajax_upload_incorrect_orders_in_cache', array($this, 'uploadIncorrectOrdersInCache'));
        add_action('wp_ajax_save_pdf', array($this, 'savePDF'));
        add_action('wp_ajax_start_cron', array($this, 'startCron'));

        add_action( 'schedule_send_email', array($this, 'scheduleSendEmailInterval'), 10, 2);
    }

    public function uploadOrdersInCache()
    {
        $dateBetween = $_POST['date_between'];

        if (!$dateBetween) wp_die(0);

        $result = get_transient($dateBetween . '_orders');

        if (!$result) {
            $order = new WooRevenueOrders();
            $ordersData = self::getOrdersByMonth($dateBetween);

            $result = $order->getMonthStatistic($dateBetween, $ordersData);
            set_transient($dateBetween . '_orders', $result);
        }

        wp_die(json_encode($result));
    }


    /**
     *  Remove cache orders by date
     *
     *  Param date_between {string}
     *
     *  Return {number}
     */
    public function removeOrdersInCache()
    {
        $dateBetween = $_POST['date_between'];

        if (!$dateBetween) wp_die(0);

        $result = delete_transient( $dateBetween . '_orders' ) ? 1 : 0;

        wp_die($result);
    }

    /**
     * Get list of the month for caching
     *
     * return object
     */
    public function getListOfTheMonth()
    {
        $order = new WooRevenueOrders();
        $months = $order->getDateListByMonth();

        //Restriction that the current month and previous not be cached
        $months = array_slice($months, 2);

        wp_die(json_encode($months));
    }

    public static function getOrdersByMonth($dateBetween)
    {
        $args = array(
            'posts_per_page' => -1,
            'date_created' => $dateBetween,
        );

        $orders = wc_get_orders($args);
        return $orders;
    }

    public function savePDF()
    {
        $orders = new WooRevenueOrders();
        $ordersStatistic = $orders->getTotalStatistic();

        $pdfGenerator = new WooRevenuePdfGenerator($ordersStatistic);
        wp_die(json_encode($pdfGenerator->get_pdf_url()));
    }

    public function uploadIncorrectOrdersInCache()
    {
        $dateBetween = $_POST['date_between'];
        $isUpdate = $_POST['is_update'];

        if (!$dateBetween) wp_die(0);

        $result = get_transient($dateBetween . '_incorrect_tax');

        if (!$result || $isUpdate) {
            $order = new WooRevenueOrders();
            $ordersData = self::getOrdersByMonth($dateBetween);

            $result = $order->getIncorrectTaxByMonth($dateBetween, $ordersData);
            set_transient($dateBetween . '_incorrect_tax', $result);
        }

        wp_die(json_encode($result));
    }

    public function startCron()
    {
        $cronData = isset($_POST['form_data']) ? $_POST['form_data'] : false;

        if (!$cronData) wp_die(0);

        $time = explode(' : ', $cronData['time']);
        $date = new DateTime();
        $seconds = $date->format('U');

        $dateStart = new DateTime();
        $dateStart->modify('+1 day');
        $dateStart->setTime($time[0], $time[1]);
        $seconds2 = $dateStart->format('U');

        $startTime = $seconds2 - $seconds;
        $result = $this->registerSchedule('schedule_send_email', $cronData, $dateStart->format('Y-m-d H:i:s'), $startTime);

        wp_die($result);

    }

    /**
     * Register CRON Schedule
     *
     * @param $name {string}
     * @param $cron_data {array}
     * @param $start_date {string}
     * @param $time_before_start {integer} in seconds
     *
     * @return int
     */
    public function registerSchedule($name, $cron_data, $start_date, $time_before_start = 0)
    {
        wp_clear_scheduled_hook($name);

        $startDate = new DateTime($start_date);
        $newDate = new DateTime($start_date);

        switch ($cron_data['interval_type']) {
            case 'interval_day' :
                $newDate->modify('+' . $cron_data['interval_day'] . ' day');
                break;
            case 'first_day':
                $newDate->modify('first day of next month');
                break;
            case 'last_day':
                $newDate->modify('last day of next month');
                break;
            case 'week_day':
                $time = explode(' : ', $cron_data['time']);
                $newDate->modify($cron_data['interval_week']);
                $newDate->setTime($time[0], $time[1]);
                break;
            case 'month_day':
                $dayOfMonthDate = new DateTime($start_date);
                $dayOfMonth = [
                    'current' => $dayOfMonthDate->format('j'),
                    'max' => $dayOfMonthDate->modify('last day of this month')->format('j'),
                    'next_max' =>$dayOfMonthDate->modify('last day of next month')->format('j')
                ];

                if ($dayOfMonth['current'] > $cron_data['interval_month']){
                    $newDate->modify('first day of next month');
                    $days = $cron_data['interval_month'] > $dayOfMonth['next_max'] ? $dayOfMonth['next_max'] - 1 : $dayOfMonth['interval_month'] - 1;
                    $newDate->modify('+' . $days . ' day');
                } else {
                    $newDate->modify('first day of this month');
                    $days = $cron_data['interval_month'] > $dayOfMonth['max'] ? $dayOfMonth['max'] - 1 : $dayOfMonth['interval_month'] - 1;
                    $newDate->modify('+' . $days . ' day');
                }

                $newDate->modify($cron_data['interval_month'] . ' day');
                break;
            default :
                return 0;
        }

        $interval = $newDate->format('U') - $startDate->format('U');

        wp_schedule_single_event( time() + (int)$interval + (int)$time_before_start, $name, array( $cron_data, $newDate->format('Y-m-d H:i:s') ) );
//        wp_schedule_single_event( time() + 60, 'schedule_send_email', array( $cron_data, $newDate->format('Y-m-d H:i:s') ) );

        return 1;
    }

    /**
     * @param $cron_data {array}
     * @param $date_start {string} - Y-m-d H:i:s
     */
    public function scheduleSendEmailInterval($cron_data, $date_start)
    {
        $options = get_option('woo_revenue_opt');
        if (isset($options['reporter_email'])) {
            $title = __('Orders table', 'woo-revenue');
            $description = __(' file with Order table', 'woo-revenue');
            $headers = array('Content-Type: text/html; charset=UTF-8');

            $orders = new WooRevenueOrders();
            $ordersStatistic = $orders->getTotalStatistic();

            $pdfGenerator = new WooRevenuePdfGenerator($ordersStatistic);
            $url = $pdfGenerator->get_pdf_url();

            $link = '<a href="' . $url . '" download>' . __('Download PDF', 'woo-revenue') . '</a>';

            wp_mail( $options['reporter_email'], $title, $link . $description, $headers);
            $this->registerSchedule('schedule_send_email', $cron_data, $date_start);
        }
    }

}