<?php
/**
 * @var $countItems {integer}
 * @var $defaultUrl {string}
 * @var $postsPerPage {integer}
 * @var $getPage
 */
?>

<?php
    $url = $defaultUrl . 'pagination=';
    $pagesCount = ceil($countItems/$postsPerPage);
?>

<section class="woo-revenue-pagination">
    <?php for ($i = 1; $i <= $pagesCount; $i++): ?>
        <a href="<?= $url . $i ?>" class="pagination-single <?= $i == $getPage + 1 ? 'active' : '' ?>"><?= $i ?></a>
    <?php endfor; ?>
</section>
