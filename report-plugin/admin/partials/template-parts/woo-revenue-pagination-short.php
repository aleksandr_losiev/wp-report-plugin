<?php
/**
 * @var $countItems {integer}
 * @var $defaultUrl {string}
 * @var $postsPerPage {integer}
 * @var $getPage
 */
?>

<?php
    $url = $defaultUrl . 'pagination=';
    $pagesCount = ceil($countItems/$postsPerPage);
?>

<section class="woo-revenue-pagination woo-revenue-pagination-short">
    <?php for ($i = 1; $i <= $pagesCount; $i++): ?>
        <?php if(
                $i == 1 ||
                $i == $pagesCount ||
                ($i >= $getPage - 4 && $i <= $getPage + 6)
        ): ?>
            <a href="<?= $url . $i ?>" class="pagination-single <?= $i == $getPage + 1 ? 'active' : '' ?>"><?= $i ?></a>
        <?php endif; ?>
    <?php endfor; ?>
</section>
