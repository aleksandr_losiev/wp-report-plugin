<?php
    $cronTime = isset($this->options['cron_time']) ? esc_attr($this->options['cron_time']) : '13 : 00';
    $cronIntervalWeek = isset($this->options['cron_interval_week']) ? esc_attr($this->options['cron_interval_week']) : 'monday';

    $cronInterval = [
        'name' => $this->plugin_options_name . '[cron_interval]',
        'value' => isset($this->options['cron_interval']) ? esc_attr($this->options['cron_interval']) : '',
        'buttons' => [
            ['label' => __('First day month', 'woo-revenue'), 'value' => 'first_day', 'isDisabled' => false],
            ['label' => __('Last day month', 'woo-revenue'), 'value' => 'last_day', 'isDisabled' => false],
            ['label' => __('Day of the week', 'woo-revenue'), 'value' => 'week_day', 'isDisabled' => false],
            ['label' => __('Day of the month', 'woo-revenue'), 'value' => 'month_day', 'isDisabled' => false],
            ['label' => __('After an interval of days', 'woo-revenue'), 'value' => 'interval_day', 'isDisabled' => false],
        ]
    ];

    $cronIntervalWeek = [
        'name' => $this->plugin_options_name . '[cron_interval_week]',
        'value' => isset($this->options['cron_interval_week']) ? esc_attr($this->options['cron_interval_week']) : 'monday',
        'options' => [
            ['value' => 'monday', 'label' => __('Monday', 'woo-revenue')],
            ['value' => 'tuesday', 'label' => __('Tuesday', 'woo-revenue')],
            ['value' => 'wednesday', 'label' => __('Wednesday', 'woo-revenue')],
            ['value' => 'thursday', 'label' => __('Thursday', 'woo-revenue')],
            ['value' => 'friday', 'label' => __('Friday', 'woo-revenue')],
            ['value' => 'saturday', 'label' => __('Saturday', 'woo-revenue')],
            ['value' => 'sunday', 'label' => __('Sunday', 'woo-revenue')],
        ]
    ];

    wp_localize_script( 'woo-revenue', 'cronData', array(
        'time'  => $cronTime,
    ) );
?>

<div class="woo-revenue-cron-data">
    <form name="cron-form-container" method="POST" action="test.php">
        <div class="cron-field">
            <p><?php _e('Send time:', 'woo-revenue') ?></p>
            <input type="text" name="<?= $this->plugin_options_name . '[cron_time]' ?>" class="timepicker" value="<?= $cronTime ?>"/>
        </div>

        <div class="cron-field cron-radio-button">
            <p><?php _e('Interval in days:', 'woo-revenue') ?></p>

            <?php foreach ($cronInterval['buttons'] as $key => $val): ?>
                <div class="single-radio">
                    <input
                        type="radio"
                        name="<?= $cronInterval['name'] ?>"
                        id="cron_interval-<?= $key ?>"
                        value="<?= $val['value'] ?>"
                        <?= $cronInterval['value'] == $val['value'] ? 'checked' : '' ?>
                        <?= $val['isDisabled'] ? 'disabled' : '' ?>
                    >
                    <label for="cron_interval-<?= $key ?>"><?= $val['label'] ?></label>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="cron-field action-field <?= $cronInterval['value'] == 'week_day' ? '' : 'hidden' ?>" data-action="week_day">
            <p><?php _e('Choose a day of the week.', 'woo-revenue') ?></p>
            <select  name="<?= $cronIntervalWeek['name'] ?>">
                <?php foreach ($cronIntervalWeek['options'] as $option) : ?>
                    <option value="<?= $option['value'] ?>" <?= $cronIntervalWeek['value'] == $option['value'] ? 'selected' : '' ?>>
                        <?= $option['label'] ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="cron-field action-field <?= $cronInterval['value'] == 'month_day' ? '' : 'hidden' ?>" data-action="month_day">
            <p><?php _e('Select the day of the month in which the email should be sent.', 'woo-revenue') ?></p>
            <input
                min="0"
                max="31"
                type="number"
                onkeydown="return false"
                name="<?= $this->plugin_options_name . '[cron_interval_month]' ?>"
                value="<?= isset($this->options['cron_interval_month']) ? esc_attr($this->options['cron_interval_month']) : 1 ?>"
            >
        </div>

        <div class="cron-field action-field <?= $cronInterval['value'] == 'interval_day' ? '' : 'hidden' ?>" data-action="interval_day">
            <p><?php _e('After how many days should send the email.', 'woo-revenue') ?> <i>(<?php _e('The countdown starts from the next day.', 'woo-revenue') ?>)</i></p>
            <input
                type="number"
                name="<?= $this->plugin_options_name . '[cron_interval_count]' ?>"
                value="<?= isset($this->options['cron_interval_count']) ? esc_attr($this->options['cron_interval_count']) : 3 ?>"
            >
        </div>

        <?php
        $AB_Text = __('Start CRON', 'woo-revenue');
        $AB_ID = 'start_cron_button';
        $AB_Data = [
            'data-text-start' => __('Preparing for the start of the CRON operation', 'woo-revenue'),
            'data-text-finish' => __('CRON successfully launched!', 'woo-revenue'),
        ];
        include 'woo-revenue-action-button.php';
        ?>
    </form>

</div>

