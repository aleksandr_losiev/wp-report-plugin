<?php
/**
 * @var $AB_Text {string}
 * @var $AB_ID {string}
 * @var $AB_Data {array}
 * @var $AB_HelperContent {string}
 */
?>

<div class="button-action action-button-block">
    <button id="<?= $AB_ID ?>"
            class="button button-secondary action-button"
            <?php foreach ($AB_Data as $data => $value): ?>
                <?= $data ?>="<?= $value ?>"
            <?php endforeach; ?>
    ><?= $AB_Text ?></button>
    <div class="action-status hidden"><?php _e('Status:', 'woo-revenue') ?> <span class="description"></span><span class="dashicons dashicons-update load-icon hidden"></span></div>
</div>