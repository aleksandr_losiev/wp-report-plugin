<?php
    $page = $_GET['page'];
?>
<div class="woo-revenue-menu">
    <a class="<?= $page === 'wrg-setting-admin' ? 'active' : '' ?>" href="/wp-admin/options-general.php?page=wrg-setting-admin"><?= __('Main settings', 'woo-revenue'); ?></a>
    <a class="<?= $page === 'wrg-setting-orders-table' ? 'active' : '' ?>" href="/wp-admin/options-general.php?page=wrg-setting-orders-table"><?= __('Orders table', 'woo-revenue'); ?></a>
    <a class="<?= $page === 'wrg-setting-incorrect-taxonomy' ? 'active' : '' ?>" href="/wp-admin/options-general.php?page=wrg-setting-incorrect-taxonomy"><?= __('Incorrect taxonomy', 'woo-revenue'); ?></a>
    <a class="<?= $page === 'wrg-setting-products-table' ? 'active' : '' ?>" href="/wp-admin/options-general.php?page=wrg-setting-products-table"><?= __('Products table', 'woo-revenue'); ?></a>
    <a class="<?= $page === 'wrg-setting-custom-table' ? 'active' : '' ?>" href="/wp-admin/options-general.php?page=wrg-setting-custom-table"><?= __('Custom table', 'woo-revenue'); ?></a>
</div>