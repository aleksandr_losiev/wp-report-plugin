<div class="wrap">
    <h2><?php _e('Woocommerce Revenue Reports - Products table','woo-revenue'); ?></h2>
    <?php include 'template-parts/woo-revenue-orders-menu.php' ?>

    <?php do_settings_sections( 'wrg-setting-products-table' ); ?>
</div>
