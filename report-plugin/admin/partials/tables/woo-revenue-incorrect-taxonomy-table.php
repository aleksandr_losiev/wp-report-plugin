<?php
    $getPage = isset($_GET['pagination']) ? $_GET['pagination'] - 1 : 0;
    $orders = new WooRevenueOrders();
    $incorrectTax = $orders->getIncorrectTax();
    $postsPerPage = 25;
    $countItems = count($incorrectTax);
    $defaultUrl = '/wp-admin/options-general.php?page=wrg-setting-incorrect-taxonomy&';

    if ($getPage){
        $incorrectTax = array_slice($incorrectTax, $postsPerPage * $getPage);
    }
?>

<style type="text/css">
    #main table{
        margin-top: 40px;
        width: 100%;
        padding: 20px 0;
        font-size: 14px;
        border-collapse: collapse;
    }
    #main table,
    #main th,
    #main td{
        border: 1px solid black;
    }
    #main table td{
        padding: 3px 5px;
    }
    #main table thead tr{
        background-color: #636363;
        color: white;
        font-weight: bold;
    }
    #main table thead tr:last-child{
        background-color: #ecd5c1;
        color: #111111;
        font-weight: normal;
    }
    #main table thead td{
        text-align: center;
    }
</style>

<div id="main">
    <div class="description-block">
        <div class="text"><?php _e('By default, orders with incorrect for the last 2 months are displayed.', 'woo-revenue') ?></div>
        <div class="actions">
            <div class="button-action action-button-block">
                <button id="incorrect-tax-save"
                        class="button button-secondary action-button"
                        data-text-start="<?php _e('Making a request to the database', 'woo-revenue') ?>"
                        data-text-finish="<?php _e('Finished! The page will reload in a few seconds.', 'woo-revenue') ?>"
                        data-is-update="0"
                ><?php _e('Download all posts and save to cache', 'woo-revenue') ?></button>
                <div class="action-status hidden">Status: <span class="description"></span><span class="dashicons dashicons-update load-icon hidden"></span></div>
            </div>

            <div class="button-action action-button-block">
                <button id="incorrect-tax-update"
                        class="button button-secondary action-button"
                        data-text-start="<?php _e('Start updating incorrect orders', 'woo-revenue') ?>"
                        data-text-finish="<?php _e('Finished! The page will reload in a few seconds.', 'woo-revenue') ?>"
                        data-is-update="1"
                ><?php _e('Update all posts', 'woo-revenue') ?></button>
                <div class="action-status hidden">Status: <span class="description"></span><span class="dashicons dashicons-update load-icon hidden"></span></div>
            </div>
        </div>
    </div>
    <table>
        <thead>
            <tr>
                <td colspan="4"><?php _e('Order Data', 'woo-revenue') ?></td>
                <td colspan="4"><?php _e('Tax', 'woo-revenue') ?></td>
                <td><?php _e('Channel', 'woo-revenue') ?></td>
            </tr>
            <tr>
                <td><?php _e('Date', 'woo-revenue') ?></td>
                <td><?php _e('ID', 'woo-revenue') ?></a></td>
                <td><?php _e('Total', 'woo-revenue') ?></td>
                <td><?php _e('Cart', 'woo-revenue') ?></td>
                <td><?php _e('Cart Tax', 'woo-revenue') ?></td>
                <td><?php _e('Ship Tax', 'woo-revenue') ?></td>
                <td><?php _e('Total Tax', 'woo-revenue') ?></td>
                <td><?php _e('Total Tax in %', 'woo-revenue') ?></td>
                <td></td>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($incorrectTax as $key => $order): ?>
            <?php if ($key == $postsPerPage ) break ; ?>
            <tr>
                <td><?= $order['date_created'] ?></td>
                <td><a href="<?= $order['url']?>"><?= $order['id']?></a></td>
                <td><?= $order['total'] ?></td>
                <td><?= $order['without_tax'] ?></td>
                <td><?= $order['cart_tax'] ?></td>
                <td><?= $order['shipping_tax'] ?></td>
                <td><?= $order['total_tax'] ?></td>
                <td><?= $order['tax_percent_round'] ?></td>
                <td><?= $order['created_via'] ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <?php
        include ADMIN_DIR . 'partials/template-parts/woo-revenue-pagination.php';
    ?>

</div>