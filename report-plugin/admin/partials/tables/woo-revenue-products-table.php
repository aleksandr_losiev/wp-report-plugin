<?php
    $products = new WooRevenueProducts();

    $getPage = isset($_GET['pagination']) ? $_GET['pagination'] - 1 : 0;
    $postsPerPage = $products->productsPerPage;
    $countItems = $products->productsCount;
    $defaultUrl = '/wp-admin/options-general.php?page=wrg-setting-products-table&';

?>

<div id="main">
    <table>
        <thead>
            <tr>
                <td><?= __('Product ID', 'woo-revenue')?></td>
                <td><?= __('Product Name', 'woo-revenue')?></td>
                <td><?= __('Product status', 'woo-revenue')?></td>
                <td><?= __('Taxonomy status', 'woo-revenue')?></td>
                <td><?= __('Taxonomy class', 'woo-revenue')?></td>
                <td><?= __('Actions', 'woo-revenue')?></td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($products->getProducts($getPage * $postsPerPage) as $product): ?>
                <tr>
                    <td><?= $product->get_id(); ?></td>
                    <td>
                        <a href="<?= $products->getEditorPermalink( $product->get_id()) ?>">
                            <?= $product->get_title(); ?>
                        </a>
                    </td>
                    <td><?= $product->get_status(); ?></td>
                    <td><?= $product->get_tax_status(); ?></td>
                    <td><?= $product->get_tax_class() ?: 'Standard'; ?></td>
                    <td>
                        <a href="<?= get_permalink( $product->get_id() )?>"><?= __('View', 'woo-revenue')?></a>
                        <a href="<?= $products->getEditorPermalink( $product->get_id()) ?>"><?= __('Edit', 'woo-revenue')?></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php
    include ADMIN_DIR . 'partials/template-parts/woo-revenue-pagination.php';
?>