<?php
    $orders = new WooRevenueOrders();

    $ordersByMonth = null;
    $orderLastDate = [];

    $getPage = isset($_GET['pagination']) ? $_GET['pagination'] - 1 : 0;
    $postsPerPage = 30;
    $countItems = $orders->getOrdersCount();
    $defaultUrl = '/wp-admin/options-general.php?page=wrg-setting-custom-table&';

//    echo '<pre>';
//    var_dump(wc_get_order( 19598 ));
//    echo '<pre>';
?>

<div id="main">
    <table>
        <thead>
            <tr>
                <td><?= __('Month', 'woo-revenue')?></td>
                <td><?= __('Revenue', 'woo-revenue')?></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="12"><?= __('Channels', 'woo-revenue')?></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="4"><?= __('Tax', 'woo-revenue')?></td>
                <td colspan="8"><?= __('Website', 'woo-revenue')?></td>
                <td colspan="3"><?= __('Amazon', 'woo-revenue')?></td>
                <td><?= __('Ebay', 'woo-revenue')?></td>
            </tr>
            <tr>
                <td></td>
                <td><?= __('Order ID', 'woo-revenue')?></td>
                <td><?= __('Net Value', 'woo-revenue')?></td>
                <td><?= __('Tax 7%', 'woo-revenue')?></td>
                <td><?= __('Tax 19%', 'woo-revenue')?></td>
                <td><?= __('Tax other (%)', 'woo-revenue')?></td>
                <td><?= __('Shipping Net', 'woo-revenue')?></td>
                <td><?= __('Shipping Tax', 'woo-revenue')?></td>
                <td colspan="9"></td>
            </tr>

            <tr>
                <td></td>
                <td><?= __('Total(Net)', 'woo-revenue')?></td>
                <td>7%</td>
                <td>19%</td>
                <td>Other (%)</td>
                <td><?= __('Total', 'woo-revenue')?></td>
                <td><?= __('Pay in advance', 'woo-revenue')?></td>
                <td><?= __('Invoice', 'woo-revenue')?></td>
                <td><?= __('PayPal', 'woo-revenue')?></td>
                <td><?= __('Amazon', 'woo-revenue')?></td>
                <td><?= __('Sofort', 'woo-revenue')?></td>
                <td><?= __('Bacs', 'woo-revenue')?></td>
                <td><?= __('Cod', 'woo-revenue')?></td>
                <td><?= __('Total', 'woo-revenue')?></td>
                <td><?= __('FBM', 'woo-revenue')?></td>
                <td><?= __('FBA', 'woo-revenue')?></td>
                <td><?= __('Ebay', 'woo-revenue')?></td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($orders->getOrders($getPage * $postsPerPage) as $order): ?>
                <?php $orderData = $orders->getOrderStatistic($order);
                    $dateBetween = $orders->getDateBetweenByDate($orderData['date_created']);
                    if ((!$ordersByMonth && !$orderLastDate) || ($orderLastDate && $dateBetween && ($orderLastDate != $dateBetween))):
                        $ordersByMonth = $orders->getMonthStatistic($dateBetween['between']);
                        $orderLastDate = $dateBetween;
                        $wt = $ordersByMonth['chanel']['website']['total']; //Web total (wt)
                    ?>
                        <tr class="month-statistic">
                            <td><?= $orderLastDate['month'] ?></td>
                            <td><?= $ordersByMonth['total'] ?></td>
                            <td><?= $ordersByMonth['tax']['tax_7'] ?></td>
                            <td><?= $ordersByMonth['tax']['tax_19'] ?></td>
                            <td><?= $ordersByMonth['tax']['tax_other'] ?></td>
                            <td><?= valueWithPercent($ordersByMonth['total'], $ordersByMonth['chanel']['website']['total']) ?></td>
                            <td><?= valueWithPercent($wt, $ordersByMonth['chanel']['website']['invoice']) ?></td>
                            <td><?= valueWithPercent($wt, $ordersByMonth['chanel']['website']['payments_advanced']) ?></td>
                            <td><?= valueWithPercent($wt, $ordersByMonth['chanel']['website']['paypal']) ?></td>
                            <td><?= valueWithPercent($wt, $ordersByMonth['chanel']['website']['amazon']) ?></td>
                            <td><?= valueWithPercent($wt, $ordersByMonth['chanel']['website']['sofortgateway']) ?></td>
                            <td><?= valueWithPercent($wt, $ordersByMonth['chanel']['website']['bacs']) ?></td>
                            <td><?= valueWithPercent($wt, $ordersByMonth['chanel']['website']['cod']) ?></td>
                            <td><?= valueWithPercent($ordersByMonth['total'], $ordersByMonth['chanel']['amazon']['total'])?></td>
                            <td></td>
                            <td></td>
                            <td><?= valueWithPercent($ordersByMonth['total'], $ordersByMonth['chanel']['ebay']['total']) ?></td>
                        </tr>
                    <?php endif;
                ?>

                <tr>
                    <td></td>
                    <td><a href="<?= $orderData['url'] ?>"><?= $orderData['id'] ?></a></td>
                    <td><?= $orderData['total'] ?></td>
                    <td><?= in_array($orderData['tax_percent_round'], [6, 7, 8]) ? $orderData['cart_tax'] : ''; ?></td>
                    <td><?= in_array($orderData['tax_percent_round'], [18, 19, 20]) ? $orderData['cart_tax'] : ''; ?></td>
                    <td><?= !in_array($orderData['tax_percent_round'], [6, 7, 8, 18, 19, 20]) ? $orderData['cart_tax'] . '&nbsp;(' . $orderData['tax_percent_round'] . '%)' : ''; ?></td>
                    <td><?= $orderData['shipping_total'] ?></td>
                    <td><?= $orderData['shipping_tax'] ?></td>
                    <td colspan="9"></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>


<?php
include ADMIN_DIR . 'partials/template-parts/woo-revenue-pagination-short.php';

function valueWithPercent($total, $value){
    return round($value, 2) . '&nbsp;(' . round($value * 100 / $total, 1) . '%)';
}
?>